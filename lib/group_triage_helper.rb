# frozen_string_literal: true

module GroupTriageHelperContext
  PACKAGE_DUE = 'in 7 days'
  PACKAGE_LABELS = ['triage-package']

  def package_summary(mentions: [], assignees: [], labels: [])
    <<~SUMMARY
      Hi, #{build_mentions(mentions)}

      This is a group or stage level triage package that aims to summarize
      the feature proposals and bugs which have not been scheduled or triaged.
      For more information please refer to the handbook:
      - https://about.gitlab.com/handbook/engineering/quality/triage-operations/index.html#triage-packages

      Scheduling the workload is a collaborative effort by the Product Managers
      and Engineering Managers for that group. Please work together to provide
      a best estimate on priority and milestone assignments. For each issue please:

      - Determine if the issue should be closed if it is no longer relevant or a
      duplicate.
      - If it is still relevant please assign either a best estimate versioned
      milestone, the %Backlog or the %"Awaiting further demand" milestone.
      - Specifically for ~bug, if there is no priority or clarity on a versioned
      milestone, please add a Priority label. Priority labels have an estimate
      SLO attached to them and help team members and the wider community understand
      roughly when it will be considered to be scheduled.
        - https://docs.gitlab.com/ee/development/contributing/issue_workflow.html#priority-labels
      - Once a milestone has been assigned please check off the box for that issue.
      - Please work with your team to complete the list by the due date set.

      {{items}}

      /label #{build_labels(labels)}
      /label #{build_labels(PACKAGE_LABELS)}
      /due #{PACKAGE_DUE}
      /assign #{build_mentions(assignees)}

      ---

      This is a group level triage package that aims to collate the latest bug reports (for frontend and otherwise) and feature proposals.
      For more information please refer to the handbook:
      - https://about.gitlab.com/handbook/engineering/quality/triage-operations/index.html#triage-packages

      ---

      If assignees or people mentioned in this individual triage package need to be amended, please edit [team-triage-package.yml](https://gitlab.com/gitlab-org/quality/triage-ops/blob/master/policies/stages/package/team-triage-package.yml).

    SUMMARY
  end

  def feature_proposal_intro
    <<~INTRO
      ### Feature Proposal Section

      For the following feature proposals. Please either close or assign either a versioned milestone, the %Backlog or the %"Awaiting further demand" milestone.

    INTRO
  end

  def bug_section_intro
    <<~INTRO
      ### Bug Section

      For the following bugs. Please either close or assign either a versioned
      milestone, the %Backlog or the %"Awaiting further demand" milestone and
      ensure that a priority label is set.

    INTRO
  end

  def bug_heatmap
    paragraph = <<~HEATMAP_PARAGRAPH
      Bugs for their priority and severity label are counted here. Every bug should
      have severity and priority labels applied. Please take a look at the bugs
      which fall into the columns indicating that the priority or severity labels
      are currently missing.
    HEATMAP_PARAGRAPH

    titled_heatmap("#### Heatmap for all bugs", paragraph)
  end

  def missed_slo_heatmap
    titled_heatmap("### Heatmap for ~missed-SLO bugs")
  end

  def unscheduled_feature_summary
    titled_summary("#### Unscheduled ~feature (non-customer)")
  end

  def unscheduled_customer_feature_summary
    titled_summary("#### Unscheduled ~feature with ~customer")
  end

  def unscheduled_ux_debt_summary
    titled_summary("#### Unscheduled UX Debt Issues")
  end

  def remaining_unscheduled_bugs_summary
    titled_summary("#### Unscheduled ~bug (non-customer)")
  end

  def remaining_unscheduled_customer_bugs_summary
    titled_summary("#### Unscheduled ~bug with ~customer")
  end

  def frontend_unscheduled_bugs_summary
    titled_summary("#### Unscheduled ~frontend ~bug (non-customer)")
  end

  def frontend_unscheduled_customer_bugs_summary
    titled_summary("#### Unscheduled ~frontend ~bug with ~customer")
  end

  def titled_heatmap(title, paragraph = nil)
    <<~HEATMAP
      #{title}

      #{paragraph}

      #{resource[:heat_map]}

      ----
    HEATMAP
  end

  def titled_summary(title)
    <<~SUMMARY

    #{title}

    {{items}}

    ----
    SUMMARY
  end

  def build_mentions(usernames)
    build_command(usernames.map(&:strip), prefix: '@')
  end

  def build_labels(labels)
    build_command(labels.map(&:strip), prefix: '~', quote: true)
  end

  def build_command(strings, prefix: '', quote: false)
    strings.map do |string|
      body =
        if quote
          %Q{"#{string}"}
        else
          string
        end

      if string.start_with?(prefix)
        body
      else
        "#{prefix}#{body}"
      end
    end.join(' ')
  end
end
