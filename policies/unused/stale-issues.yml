.common_conditions:
  label_exemptions: &label_exemptions
    forbidden_labels:
      - technical debt
      - Accepting merge requests
      - backstage
      - security
      - customer
      - customer+
      - test
      - development guidelines
      - schema redesign
      - direction
      - static analysis
  team_author: &team_author
    author_member:
      source: group
      condition: member_of
      # gitlab-org
      source_id: 9970
  community_author: &community_author
    author_member:
      source: group
      condition: not_member_of
      # gitlab-org
      source_id: 9970

.common_rules: &common_rules
  limits:
    most_recent: 25

.common_actions_labels: &common_actions_labels
  labels:
    - awaiting feedback
    - auto updated

.common_summary_rule_actions: &common_summary_rule_actions
  item: |
    - [ ] ##{resource[:iid]} {{title}} {{labels}}
  summary: |
    {{items}}

.common_summary_rule_text: &common_summary_rule_text
  summary: |
    Hi triage team (@gitlab-org/triage),

    The following issues have been auto-updated by the bot.

    Please see the following file for the relevant policies making up this package:
    - #{ENV['CI_PROJECT_URL']}/blob/#{ENV['CI_COMMIT_SHA']}/policies/stale-issues.yml

    ----

    {{items}}

    ----

    The due date has been set for one week from now. This is to give time for:
    * The author time to respond and update their issue
    * The triage team to review community reports

    When this issue is due, we'd like to ask you to:
    * Close the referenced issue if it is no longer relevant
    * If still valid, follow the normal triage procedure to flag to the relevant team

    Thanks for your help!

    /due in 7 days
    /label ~"triage\-package" ~Quality

    ---

    This is a Quality team triage package that aims to collate the stale issues. For more information please refer to the handbook:
    - https://about.gitlab.com/handbook/engineering/quality/guidelines/triage-operations/#current-packages

resource_rules:
  issues:
    summaries:
      - name: Stale unscheduled issues requiring attention
        actions:
          summarize:
            title: Stale unscheduled issues requiring attention
            <<: *common_summary_rule_text
        rules:
          - name: Remind stale unscheduled issues (team)
            <<: *common_rules
            conditions:
              date:
                attribute: updated_at
                condition: older_than
                interval_type: months
                interval: 6
              milestone: none
              state: opened
              <<: *label_exemptions
              <<: *team_author
            actions:
              comment: |
                Hi {{author}},

                We're posting this message because this issue meets the following criteria:

                * No activity in the past 6 months (since {{updated_at}})
                * No milestone (unscheduled)

                We'd like to ask you to help us out and determine how we should act on this issue.

                If this issue is reporting a bug, please can you:

                - Attempt to reproduce on the latest version of GitLab or GitLab.com, to help us to understand whether the bug still needs attention.
                - If this bug is still relevant please ping an Engineering Manager to possibly add to the %Backlog.

                If this issue is proposing a new feature, please can you:

                - Verify whether the feature proposal is still relevant.
                - If this feature proposal is still relevant please ping a Product Manager or possibly add to the %Backlog

                Thanks for your help! :heart:

                ----

                You are welcome to help [improve this comment](#{ENV['CI_PROJECT_URL']}/blob/#{ENV['CI_COMMIT_SHA']}/policies/stale-issues.yml).
              summarize:
                <<: *common_summary_rule_actions
          - name: Collect stale unscheduled issues (community)
            <<: *common_rules
            conditions:
              date:
                attribute: updated_at
                condition: older_than
                interval_type: months
                interval: 6
              milestone: none
              state: opened
              <<: *label_exemptions
              <<: *community_author
            actions:
              summarize:
                <<: *common_summary_rule_actions

      - name: Stale unlabelled issues requiring attention
        actions:
          summarize:
            title: Stale unlabelled issues requiring attention
            <<: *common_summary_rule_text
        rules:
          - name: Remind stale unlabelled issues (team)
            <<: *common_rules
            conditions:
              date:
                attribute: updated_at
                condition: older_than
                interval_type: months
                interval: 3
              labels:
                - none
              state: opened
              <<: *team_author
            actions:
              comment: |
                Hi {{author}},

                First of all, thank you for raising an issue to help improve the GitLab product. We're sorry about this, but this particular issue has gone unnoticed for quite some time.

                We're posting this message because this issue meets the following criteria:

                * No activity in the past 3 months (since {{updated_at}})
                * Unlabelled

                We'd like to ask you to help us out and determine how we should act on this issue.

                If this issue is reporting a bug, please can you:

                - Attempt to reproduce on the latest version of GitLab or GitLab.com, to help us to understand whether the bug still needs attention.
                - If this bug is still relevant please ping an Engineering Manager to possibly add to the %Backlog.

                If this issue is proposing a new feature, please can you:

                - Verify whether the feature proposal is still relevant.
                - If this feature proposal is still relevant please ping a Product Manager or possibly add to the %Backlog

                Thanks for your help! :heart:

                ----

                You are welcome to help [improve this comment](#{ENV['CI_PROJECT_URL']}/blob/#{ENV['CI_COMMIT_SHA']}/policies/stale-issues.yml).
              summarize:
                <<: *common_summary_rule_actions
          - name: Collect stale unlabelled issues (community reported)
            <<: *common_rules
            conditions:
              date:
                attribute: updated_at
                condition: older_than
                interval_type: months
                interval: 3
              labels:
                - none
              state: opened
              <<: *community_author
            actions:
              summarize:
                <<: *common_summary_rule_actions

      - name: Stale bugs requiring attention
        actions:
          summarize:
            title: Stale bugs requiring attention
            summary: |
              Hi triage team (@gitlab-org/triage),

              The following issues have been auto-updated by the bot.

              Please see the following file for the relevant policies making up this package:
              - #{ENV['CI_PROJECT_URL']}/blob/#{ENV['CI_COMMIT_SHA']}/policies/stale-issues.yml

              ----

              {{items}}

              ----

              The due date has been set for one week from now. This is to give time for:
              * The author time to respond and update their issue
              * The triage team to review community reports

              When this issue is due, we'd like to ask you to:
              * Close the referenced issue if it is no longer relevant
              * If still valid, follow the normal triage procedure to flag to the relevant team

              Thanks for your help!

              /due in 7 days
              /label ~"triage\-package" ~Quality
              /assign @meks @at.ramya @tpazitny

              ---

              This is a Quality team triage package that aims to collate the stale issues. For more information please refer to the handbook:
              - https://about.gitlab.com/handbook/engineering/quality/guidelines/triage-operations/#current-packages
        rules:
          - name: Remind stale bugs (team)
            <<: *common_rules
            conditions:
              date:
                attribute: updated_at
                condition: older_than
                interval_type: months
                interval: 6
              labels:
                - bug
              milestone: none
              state: opened
              <<: *label_exemptions
              <<: *team_author
            actions:
              summarize:
                <<: *common_summary_rule_actions
              comment: |
                Hi {{author}},

                First of all, thank you for raising an issue to help improve the GitLab product. We're sorry about this, but this particular issue has gone unnoticed for quite some time.

                We're posting this message because this issue meets the following criteria:

                * Issue is open
                * No activity in the past 6 months (since {{updated_at}})
                * Labelled as a bug. Currently has the following labels: {{labels}}
                * Unscheduled (no milestone)

                We'd like to ask you to help us out and determine how we should act on this issue.

                Because this issue is reporting a bug, please can you:

                - Attempt to reproduce on the latest version of GitLab or GitLab.com, to help us to understand whether the bug still needs attention.
                - If this bug is still relevant please ping an Engineering Manager to possibly add to the %Backlog.

                Thanks for your help! :heart:

                ----

                You are welcome to help [improve this comment](#{ENV['CI_PROJECT_URL']}/blob/#{ENV['CI_COMMIT_SHA']}/policies/stale-issues.yml).
          - name: Collect stale bugs (community reported)
            <<: *common_rules
            conditions:
              date:
                attribute: updated_at
                condition: older_than
                interval_type: months
                interval: 6
              labels:
                - bug
              milestone: none
              state: opened
              <<: *label_exemptions
              <<: *community_author
            actions:
              summarize:
                <<: *common_summary_rule_actions

      - name: Stale support requests requiring attention
        actions:
          summarize:
            title: Stale support requests requiring attention
            <<: *common_summary_rule_text
        rules:
          - name: Remind stale support requests
            <<: *common_rules
            conditions:
              date:
                attribute: updated_at
                condition: older_than
                interval_type: months
                interval: 3
              labels:
                - support request
              milestone: none
              state: opened
              forbidden_labels:
                - awaiting feedback
            actions:
              <<: *common_actions_labels
              comment: |
                Hi {{author}},

                This issue was labelled as a support request.

                This issue will be marked for closure, as it meets the following criteria:
                * Issue is open
                * No activity in the past 3 months (since {{updated_at}})
                * Labelled as a support request. Currently has the following labels: {{labels}}
                * Unscheduled (no milestone)

                We'd like to ask you to help us out and determine whether this issue should remain open.

                Please could you revisit the issue and provide an update if it is still relevant.

                Please mention the Triage team (`@gitlab-org/triage`) for help with applying labels

                Thanks for your help! :heart:

                ----

                You are welcome to help [improve this comment](#{ENV['CI_PROJECT_URL']}/blob/#{ENV['CI_COMMIT_SHA']}/policies/stale-issues.yml).
              summarize:
                <<: *common_summary_rule_actions
